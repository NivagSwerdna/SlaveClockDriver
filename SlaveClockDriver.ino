// include the library code:

#define ONBOARD_LED    13
#define CLOCK_DATA_IN  2
#define CLOCK_POWER_CONTROL 3

#define HBRIDGE_ENABLE      A5
#define HBRIDGE_DIRECTION_1 12
#define HBRIDGE_DIRECTION_2 11

#define BTN_1  A3
#define BTN_2  A2
#define BTN_3  A1

#include "dcf77.h"
#include <EEPROM.h>
#include <LiquidCrystal.h>


uint8_t sample_input_pin() {
  const uint8_t sampled_data = digitalRead(CLOCK_DATA_IN);
  digitalWrite(ONBOARD_LED, sampled_data);
  return sampled_data;
}



LiquidCrystal lcd(9, 8, 7, 6, 5, 4);

#define VERSION "v3.11"
char* build_date = __DATE__;

// EEPROM

#define EEPROM_OFFSET 256
#define EEPROM_OFFSET_DELTA 256+720

int clock_offset = 0;
int clock_hrs;
int clock_mins;
int delta = -1;

/**
 * Calculate clock_hrs and clock_mins from clock_offset (after applying advance (minutes))
 */
void  calculateClockWithAdvance(int advance) {

  clock_offset += advance;
  if (clock_offset < 0) clock_offset = (12 * 60 - 1);
  if (clock_offset >= (12 * 60)) clock_offset = 0;

  clock_hrs = clock_offset / 60;
  clock_mins = clock_offset - clock_hrs * 60;
}

int eeprom_clock_offset;
int eeprom_delta;


void zapEEPROM() {

  clock_offset = 0;
  eeprom_clock_offset = 0;

  // Default to 00:00
  EEPROM.write(EEPROM_OFFSET, 255);

  for (int i = (EEPROM_OFFSET + 1) ; i < (EEPROM_OFFSET + (12 * 60)) ; i++) {
    EEPROM.write(i, 0);
  }

  eeprom_delta = 0;
  EEPROM.write(EEPROM_OFFSET_DELTA, eeprom_delta + 128);
  delta = eeprom_delta;
}

void findClockTime() {

  eeprom_clock_offset = 0;

  int numZeros = 0;
  int num255s = 0;

  for (int i = EEPROM_OFFSET ; i < (EEPROM_OFFSET + (12 * 60)) ; i++) {

    int value = EEPROM.read(i);

    if (value == 255) {
      eeprom_clock_offset = i - 256;
      num255s++;
    } else if (value == 0) {
      numZeros++;
    }
  }

  eeprom_delta = EEPROM.read(EEPROM_OFFSET_DELTA) - 128;
  delta = eeprom_delta;

  if ((numZeros + num255s) == (12 * 60)) {
    Serial.println("EEPROM OK");
    clock_offset = eeprom_clock_offset;
  } else
  {
    // EEPROM looks like it was not initialised or got corrupted during a power fail
    Serial.print("EEPROM Bad: ");
    Serial.print(numZeros);
    Serial.print(" ");
    Serial.print(num255s);
    Serial.print(" ");
    Serial.println((numZeros + num255s));

    // Init EEPROM
    zapEEPROM();
  }

  calculateClockWithAdvance(0);
}


void saveMode1() {
  if (eeprom_clock_offset != clock_offset) {
    EEPROM.write(eeprom_clock_offset + EEPROM_OFFSET, 0);
    EEPROM.write(clock_offset + EEPROM_OFFSET, 255);
    eeprom_clock_offset = clock_offset;
  }
}

void saveMode3() {
  if (eeprom_delta != delta) {
    EEPROM.write(EEPROM_OFFSET_DELTA, delta + 128);
    eeprom_delta = delta;
  }
}

void displayClockTime() {
  if (clock_hrs < 10) lcd.print("0");
  lcd.print(clock_hrs);
  lcd.print(":");
  if (clock_mins < 10) lcd.print("0");
  lcd.print(clock_mins);
}

int radio_time_available = false;

// BUTTONS

int buttons[3];
int buttons_last[3];
unsigned long  last_button_time[3];

int events = 0;

#define BTN_LONGPRESS 4
#define BTN_DOWN 1
#define BTN_UP   2

#define SHIFT_BTN1 (0)
#define SHIFT_BTN2 (3)
#define SHIFT_BTN3 (6)

boolean renderRequired = true;
unsigned long lastRendered;
boolean viewLockTime = false;     // A flag to indicate user wants to see wait time during startup

void determineButtonEvents() {

  int old_events = events;

  unsigned long now = millis();

  for (int i = 0; i < 3; i++) {

    int shift = i * 3;

    buttons_last[i] = buttons[i];

    int pin;
    switch (i) {
      case 0:
        pin = BTN_1;
        break;
      case 1:
        pin = BTN_2;
        break;
      case 2:
        pin = BTN_3;
        break;
    }

    buttons[i] = 1 - digitalRead(pin);

    // Look for a change
    if (buttons[i] != buttons_last[i]) {
      if (now - last_button_time[i] >= 50) {  // 50mS Debounce
        last_button_time[i] = now;

        if (buttons[i]) {
          events |= (BTN_DOWN << shift);
        } else
        {
          events |= (BTN_UP << shift);
        }
      }
    } else // Same as before
    {
      if (buttons[i] && (now - last_button_time[i] >= 1000)) { // 1 second Long Time
        events |= (BTN_LONGPRESS << shift);
      }
    }
  }

  if (old_events != events) renderRequired = true;
}


int required_offset;
int required_hrs;
int required_mins;

void calculateRequiredTime( const Clock::time_t &now) {
  required_hrs = bcd_to_int(now.hour);
  if (required_hrs >= 12) required_hrs -= 12;
  required_hrs -= delta; // OFFSET
  if (required_hrs < 0) required_hrs += 12;
  required_mins = bcd_to_int(now.minute);
  required_offset = required_hrs * 60 + required_mins;
}


void LCDpaddedPrint(BCD::bcd_t n) {
  lcd.print(n.digit.hi);
  lcd.print(n.digit.lo);
}


int mode = 0;
int wait_time = 5; // Example only
int run_mode = 0; // 0=RUN(OK), 1=MOVE, 2=WAIT
uint8_t radio_state;
int manual_mode = 0; // 0=Idle, 1=+ve, 2=-ve

Clock::time_t now;

void updateDisplay() {

  if (!renderRequired) return;

  lcd.setCursor(0, 0);
  switch (mode) {
    case 0:
    case 1:
    case 3:
      displayClockTime();
      break;
    case 2:
      lcd.print(" ");
      if (delta >= 0) lcd.print("+");
      lcd.print(delta);
      lcd.print("  ");
      break;
    case 4:
      lcd.print(VERSION);
      break;
  }

  lcd.setCursor(0, 1);
  switch (mode) {
    case 0:
      if (run_mode == 0) {
        lcd.print(" RUN   ");
      } else if (run_mode == 1) {
        lcd.print(" MOVE  ");
      } else
      {
        lcd.print("WAIT ");
        lcd.print(wait_time);
        if (wait_time < 10) lcd.print(" ");
      }
      break;
    case 1:
      lcd.print(" SET   ");
      break;
    case 2:
      lcd.print("TZDIFF  ");
      break;
    case 3:
      switch (manual_mode) {
        case 1:
          lcd.print(" +ve   ");
          break;
        case 2:
          lcd.print(" -ve   ");
          break;
        default:
          lcd.print("MANUAL ");
          break;
      }
      break;
    case 4:
      lcd.print("VERSION");
      break;
  }

  switch (mode) {
    case 1:
      lcd.setCursor(6, 0);
      lcd.print("  Match   ");
      lcd.setCursor(6, 1);
      lcd.print("Wall Clock");
      break;
    case 4:
      lcd.setCursor(6, 0);
      lcd.print("  Built on");
      lcd.setCursor(8, 1);
      lcd.print(" ");
      lcd.print(build_date[4]);
      lcd.print(build_date[5]);
      lcd.print(build_date[0]);
      lcd.print(build_date[1]);
      lcd.print(build_date[2]);
      lcd.print(build_date[9]);
      lcd.print(build_date[10]);
      break;
    default:

      if ((!radio_time_available) && (!viewLockTime)) {
        lcd.setCursor(6, 0);
        lcd.print("  WAIT FOR");
        lcd.setCursor(8, 1);
        lcd.print("  RADIO ");
      } else {

        // Radio Time is available

        if (viewLockTime) {

          // Show received Radio Time  (Mainflingen time if from DCF77)

          lcd.setCursor(6, 0);
          if (radio_time_available) {
            switch (radio_state) {
              case Clock::useless: lcd.print("u"); break;
              case Clock::dirty:   lcd.print("d"); break;
              case Clock::synced:  lcd.print("S"); break;
              case Clock::locked:  lcd.print("L"); break;
              default:  lcd.print("?"); break;
            }
          }
          else {
            // No clock state so don't show a confusing 'u' until we have a locked signal
            lcd.print(" ");
          }
          lcd.print(" ");
          LCDpaddedPrint(now.hour);
          lcd.print(":");
          LCDpaddedPrint(now.minute);
          lcd.print(":");
          LCDpaddedPrint(now.second);

          lcd.setCursor(8, 1);
          LCDpaddedPrint(now.day);
          lcd.print('/');
          LCDpaddedPrint(now.month);
          lcd.print('/');
          LCDpaddedPrint(now.year);
        }
        else
        {
          // Show Required Time i.e. 12hr required time with Delta Applied

          lcd.setCursor(6, 0);
          lcd.print(" ");
          lcd.print(" ");
          if (required_hrs < 10) lcd.print("0");
          lcd.print(required_hrs);
          lcd.print(":");
          if (required_mins < 10) lcd.print("0");
          lcd.print(required_mins);
          lcd.print(":");
          LCDpaddedPrint(now.second);

          lcd.setCursor(8, 1);
          lcd.print(F("        "));
        }
      }

      break;
  }
  renderRequired = false;
  lastRendered = millis();
}

unsigned long last_repeat;

int autosave_required;
unsigned long last_autosave;

void processButtonEvents() {

  unsigned long now = millis();

  // Mode Change
  if (events & (BTN_LONGPRESS << SHIFT_BTN1)) {

    // Reset all Events
    events &= !(BTN_LONGPRESS << SHIFT_BTN1);
    last_button_time[0] = now;

    // Leaving Mode
    if (mode == 1) {
      saveMode1();
    } else if (mode == 3) {
      saveMode3();
    }

    mode++;
    mode = mode % 5;

    // Entering Mode
    if (mode == 1) {
      findClockTime();
      autosave_required = 0;
      last_autosave = now;
    }

    renderRequired = true;

  } else
  {
    if (mode == 0) {
      if (events & (BTN_DOWN << SHIFT_BTN2)) {
        events ^= (BTN_DOWN << SHIFT_BTN2);
        viewLockTime = !viewLockTime;
        renderRequired = true;
      }
      if (events & (BTN_DOWN << SHIFT_BTN3)) {
        events ^= (BTN_DOWN << SHIFT_BTN3);
        viewLockTime = !viewLockTime;
        renderRequired = true;
      }
    } else if (mode == 1) {
      int advance = 0;

      if (events & (BTN_UP << SHIFT_BTN2)) {
        events &= !(BTN_LONGPRESS << SHIFT_BTN2);
      }
      else if (events & (BTN_UP << SHIFT_BTN3)) {
        events &= !(BTN_LONGPRESS << SHIFT_BTN3);
      }
      else if (events & (BTN_LONGPRESS << SHIFT_BTN2)) {
        if (now - last_repeat > 200) {
          last_repeat = now;
          advance = ((now - last_button_time[1]) > 2000) ? -10 : -1;
        }
      }
      else if (events & (BTN_LONGPRESS << SHIFT_BTN3)) {
        if (now - last_repeat > 200) {
          last_repeat = now;
          advance = ((now - last_button_time[2]) > 2000) ? 10 : 1;
        }
      }
      else if (events & (BTN_DOWN << SHIFT_BTN2)) {
        events ^= (BTN_DOWN << SHIFT_BTN2);
        advance = -1;

      }
      else if (events & (BTN_DOWN << SHIFT_BTN3)) {
        events ^= (BTN_DOWN << SHIFT_BTN3);
        advance = 1;
      }

      if (advance != 0)
      {
        calculateClockWithAdvance(advance);
        if (!autosave_required) last_autosave = now;
        autosave_required = 1;
        updateDisplay();
      }

      if (autosave_required && (now - last_autosave > 5000)) {
        autosave_required = 0;
        last_autosave = now;
        saveMode1();
      }
    } else if (mode == 2) {

      int advance = 0;

      // Delta Adjustment
      if (events & (BTN_DOWN << SHIFT_BTN2)) {
        events ^= (BTN_DOWN << SHIFT_BTN2);
        advance = -1;
      }
      else if (events & (BTN_DOWN << SHIFT_BTN3)) {
        events ^= (BTN_DOWN << SHIFT_BTN3);
        advance = 1;
      }

      if (advance != 0)
      {
        delta += advance;

        // Clamp between -11 and +11
        if (delta < -11) delta = -11;
        if (delta > 11) delta = 11;

        if (!autosave_required) last_autosave = now;
        autosave_required = 1;
        renderRequired = true;
      }

      if (autosave_required && (now - last_autosave > 5000)) {
        autosave_required = 0;
        last_autosave = now;
        saveMode3();
      }
    } else if (mode == 3) {
      if (events & (BTN_DOWN << SHIFT_BTN2)) {
        events ^= (BTN_DOWN << SHIFT_BTN2);

        // Indicate we are advancing
        manual_mode = 1;
        renderRequired = true;
        updateDisplay();

        // Send signal but don't advance clock
        advanceClock(0);

        // Indicate we are done
        manual_mode = 0;
        renderRequired = true;
        updateDisplay();
      }
      else if (events & (BTN_DOWN << SHIFT_BTN3)) {
        events ^= (BTN_DOWN << SHIFT_BTN3);

        // Indicate we are advancing
        manual_mode = 1;
        renderRequired = true;
        updateDisplay();

        // Send signal but do advance clock
        advanceClock(1);

        // Indicate we are done
        manual_mode = 0;
        renderRequired = true;
        updateDisplay();
      }
    }
  }

}

unsigned long start_mills;

void setup() {

  pinMode(ONBOARD_LED, OUTPUT);   // LED
  digitalWrite(ONBOARD_LED, LOW);   // Enable MSF Receiver

  pinMode(CLOCK_DATA_IN, INPUT_PULLUP);     // Data In

  pinMode(CLOCK_POWER_CONTROL, OUTPUT);     // Power Control to MSF Receiver
  digitalWrite(CLOCK_POWER_CONTROL, LOW);   // Enable MSF Receiver

  // H-Bridge
  pinMode(HBRIDGE_ENABLE, OUTPUT);     // Power Control to MSF Receiver
  pinMode(HBRIDGE_DIRECTION_1, OUTPUT);     // Power Control to MSF Receiver
  pinMode(HBRIDGE_DIRECTION_2, OUTPUT);     // Power Control to MSF Receiver

  // Set initial direction but disable
  digitalWrite(HBRIDGE_ENABLE, LOW);   // H-Bridge Off
  digitalWrite(HBRIDGE_DIRECTION_1, HIGH);   // Direction
  digitalWrite(HBRIDGE_DIRECTION_2, LOW);   // Direction

  // Buttons
  pinMode(BTN_1, INPUT_PULLUP);
  pinMode(BTN_2, INPUT_PULLUP);
  pinMode(BTN_3, INPUT_PULLUP);

  Serial.begin(19200);

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.clear();

  // Determine Clock Time including Offset and Delta using value saved in EEPROM
  findClockTime();

  // Adjust H-Bridge direction since the clock will ignore next impulse of sent in wrong direction and
  // then we will always be out of sync.  Polarity of wiring H-Bridge is therefore SIGNIFICANT!
  {
    int initialDirection = clock_offset % 2;
    digitalWrite(HBRIDGE_DIRECTION_1, initialDirection);   // Direction
    digitalWrite(HBRIDGE_DIRECTION_2, !initialDirection);   // Direction
  }

  updateDisplay();

  using namespace Clock;

  // Preserve Timer0 flags as we don't want DCF77 Software disabling it
  uint8_t SAVE = TIMSK0;

  DCF77_Clock::setup();
  DCF77_Clock::set_input_provider(sample_input_pin);

  // Restore Timer0 to operation so we can use __delay__, __millis__ etc although accept this might introduce some jitter to the
  // DCF77 software
  TIMSK0 = SAVE;

  // Record Starting Time
  start_mills = millis();
}

uint8_t last_rendered_sec = -1;

boolean waitingForLock = true;

void advanceClock(int amount) {

  //  Flip Direction
  uint8_t currentDirection = digitalRead(HBRIDGE_DIRECTION_1);
  digitalWrite(HBRIDGE_DIRECTION_1, !currentDirection);   // Direction Free
  digitalWrite(HBRIDGE_DIRECTION_2, currentDirection);   // Direction Free

  digitalWrite(HBRIDGE_ENABLE, HIGH);   // H-Bridge On

  if (amount != 0) {
    // Advance offset and save to EEPROM
    EEPROM.write(clock_offset + EEPROM_OFFSET, 0);

    // Increment one minute and recalculate
    calculateClockWithAdvance(amount);

    // Save new value to EEPROM
    EEPROM.write(clock_offset + EEPROM_OFFSET, 255);
  }
  
  delay(90);
  digitalWrite(HBRIDGE_ENABLE, LOW);   // H-Bridge Off

}

void loop() {

  determineButtonEvents();
  processButtonEvents();
  updateDisplay();

  unsigned long currentTime = millis();

  // Check to see if we have the correct time yet
  if (waitingForLock) {
    radio_state = DCF77_Clock::get_clock_state();
    if ((radio_state != Clock::useless) && (radio_state != Clock::dirty)) {
      waitingForLock = false;
      radio_time_available = true;
    }
  }

  if (waitingForLock) {

    // Whilst waiting for the radio we just count up elapsed time
    // since we started to get a feel for how long we have been waiting...

    unsigned long delta = currentTime - start_mills;
    delta /= 1000;

    uint8_t hr2 = delta / (60 * 60);
    delta -= hr2 * (60 * 60);
    hr2 %= 24;
    uint8_t min2 = delta / 60;
    delta -= min2 * 60;
    uint8_t sec2 = delta;

    now.second.digit.hi = sec2 / 10;
    now.second.digit.lo = sec2 % 10;
    now.minute.digit.hi = min2 / 10;
    now.minute.digit.lo = min2 % 10;
    now.hour.digit.hi = hr2 / 10;
    now.hour.digit.lo = hr2 % 10;

    if (last_rendered_sec != sec2) {
      last_rendered_sec = sec2;
      renderRequired = true;
    }
  } else
  {
    boolean performAction = (last_rendered_sec != now.second.val);

    // Got our lock so track Radio time now...
    DCF77_Clock::read_current_time(now);

    // This is the main processing loop... we have radio time and we know local time so we can figure out what to do next!

    if (performAction) {
      // Limit work to once per second so we don't end up driving the clock beyond it's physical limits

      if (mode == 0) {

        // Normal Mode 0 : RUN
        calculateRequiredTime(now);

        int deltaT = required_offset - clock_offset;
        if (deltaT == 0) {
          run_mode = 0;
        }
        else {
          int fwdDiff = required_offset - clock_offset;
          if (fwdDiff < 0) fwdDiff += 60 * 12;
          int bwdDiff = clock_offset - required_offset;
          if (bwdDiff < 0) bwdDiff += 60 * 12;

          if (bwdDiff > 12) {
            advanceClock(1);
            run_mode = 1; // Advancing
          } else
          {
            wait_time = bwdDiff;
            run_mode = 2;
          }
        }
      }
    }

    // If we did an action then re-render and defer next action until next second
    if (performAction) {
      last_rendered_sec = now.second.val;
      renderRequired = true;
    }
  }
}


