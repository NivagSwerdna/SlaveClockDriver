## Software to support Pragotron Slave Clock

* https://aardvarklabs.wordpress.com/2016/05/21/pragotron-slave-clock/

### Hardware Requirements

* Arduino UNO (with Crystal Oscillator)
* MSF60 or DCF77 radio module
* H-Bridge
* DC-DC Converter to provide 24V
* LCD Display
* Trio of buttons